var apiUrl="https://app.leadpipe.io/api/v1/";
var encodedData = "";
var currentDomain = "";
var currentCompanyId = "";
var userId = "";

refreshLocalData();
refreshDomain();

chrome.browserAction.onClicked.addListener(function(){
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
        chrome.tabs.sendMessage(tabs[0].id, {action: "toggleSidebar"}, function(response){});
        refreshLocalData();
        refreshDomain();
    });
});
function refreshLocalData() {
    chrome.storage.local.get('isLoggedIn', function (result) {
        if (result.isLoggedIn) {
            chrome.storage.local.get('userLoginData', function (res) {
                encodedData = res.userLoginData;
            });
            chrome.storage.local.get('userId', function (res) {
                userId = res.userId;
            });
        }
    });
}

//ініціалізую змінну із доменом із chrome.storage.local яку заносить туди content.js спочатку.
function refreshDomain() {
    chrome.storage.onChanged.addListener(function (callback) {
        chrome.storage.local.get('currentDomain', function (result) {
            currentDomain = result.currentDomain;
        });
    });
}
//----------------------

//заношу дані про юзера у local.storage після вдалої авторизації, щоб уникнути повторного введення
function updateUserData(userLoginData, someId){
  chrome.storage.local.set({'userLoginData': userLoginData, 'isLoggedIn': true, 'userId': someId});
}
//----------------------

//стираю дані про юзера
function removeUserData(){
  chrome.storage.local.set({'userLoginData': null, 'isLoggedIn': false, 'userId':null});
}
//-----------



//дістаю id списку із компаніями відповідного до user.id

function addComToList(listId, comId){
    $.ajax({
        method: "POST",
        dataType: "json",
        url: apiUrl+"Enumerations/"+listId+"/company",
        data:{
            'ids' : [comId]
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Basic " + encodedData);
        },
        success:function (response) {
            var message = "Company added to track list successfully, now you will by notified about its every new step";
            port.postMessage({response: 2, domainTotal: message});

        },
        error: function (responseData){
            console.log("Its error: " + responseData.status + ' || ' + responseData.responseText);
        }
    });
}

//---------------------------

//Listener для реагування на команди index.js
chrome.runtime.onConnect.addListener(function(port) {
  console.assert(port.name == "startSession");
  port.onMessage.addListener(function(msg) {
    switch(msg.command){
      case 0:{
        removeUserData();
        port.postMessage({response:0});
        break;
      }
      case 1:{
        encodedData = btoa(msg.userName  + ':' + msg.userPass);
        $.ajax({
              method: "GET",
              dataType: "json",
              url: apiUrl+"App/user",

              beforeSend: function (xhr) {
                xhr.setRequestHeader ("Authorization", "Basic " + encodedData);
              },
              success:function (response) {
                userId = response['user'].id;
                updateUserData(encodedData, userId);
                port.postMessage({response : 1, totalData: "", userExists : true});
              },
              error: function (xhr, status, error){
                port.postMessage({response : 1, userExists : false});
              }
            });
            break;
          }
      case 2:{ //check the domain
          refreshDomain();
          if(currentDomain != "") {
              $.ajax({
                  method: "GET",
                  dataType: "html",
                  url: "http://localhost/dataGateway.php",
                  data: {
                      'domain': currentDomain,
                      'extensionKey': true
                  },
                  success: function (domainTotal) {
                      port.postMessage({response: 2, domainTotal: domainTotal});
                  },
                  error: function (responseData) {
                      console.log("Its error: " + responseData.status + " || " + responseData.responseText);
                  }
              });
          }
          else{
              var message = "Please go to some company website and then press the leadpipe icon or if you are already there, then refresh the webpage(press 'F5')";
              port.postMessage({response: 2, domainTotal: message});
          }
            break;
          }
      case 3:{ //Registration
          $.ajax({
              method: "POST",
              dataType: "text",
              url: "http://localhost/dataGateway.php",
              data:{
                  'userEmail' : msg.userEmail,
                  'extensionKey' : true
              },
              success:function (response) {
                  var responseParsed = JSON.parse(response);
                  updateUserData(btoa(responseParsed.userName+":"+responseParsed.userPass),responseParsed.id);
                  port.postMessage({response : 1, userExists : true});
              },
              error: function (responseData){
                  console.log("Its error: " + responseData.status + ' || ' + responseData.responseText);
              }
          });
        break;
      }
        case 4:{ //AddToTrackList
            $.ajax({
                method: "GET",
                dataType: "json",
                url: apiUrl+"Enumerations?where[0][field]=assignedUserId&where[0][type]=equals&where[0][value]="+userId,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader ("Authorization", "Basic " + encodedData);
                },
                success: function (response) {
                    addComToList(response.list[0].id, msg.comId);
                },
                error: function (responseData) {
                    console.log("Its error: " + responseData.status + " || " + responseData.responseText);
                    listId = "NONE(error)";
                }
            });
            break;
        }
      }
    });
  });
