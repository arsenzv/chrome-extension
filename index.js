var port = chrome.runtime.connect({name: "startSession"});
var authUI = document.getElementById("authUI");
var companyUI = document.getElementById("companyUI");
var loadingUI = document.getElementById("loadingUI");
var entireUI = document.getElementById("entireUI");
var authInvalid = document.getElementById("authInvalid");
var registerUI = document.getElementById("registerUI");


//checking storage on if the user was already loggedin or no...
chrome.storage.local.get('isLoggedIn', function(result){
  if (result.isLoggedIn) {
      loadingUI.style.display = "flex";
      entireUI.style.display = "none";
      port.postMessage({command: 2});
  }else
    authUI.style.display = "initial";
    registerUI.style.display = "initial";
});

//--------------


document.getElementById("closeExtension").addEventListener("click", function () {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
        chrome.tabs.sendMessage(tabs[0].id, {action: "toggleSidebar"}, function(response){
        });
    });
});

document.getElementById("addToListBtn").addEventListener("click", function(){
  var someId = document.getElementById("comId").value;
  port.postMessage({command: 4, comId: someId});
});

document.getElementById("submitLogoutBtn").addEventListener("click", function(){
    authInvalid.innerHTML="";
  port.postMessage({command: 0});
});

document.getElementById("submitLoginBtn").addEventListener("click", function(){
  authInvalid.innerHTML="";
     entireUI.style.display = "none";
    loadingUI.style.display = "flex";
  var userName = document.getElementById("userName").value;
  var userPass = document.getElementById("userPass").value;
  port.postMessage({command: 1, userName: userName, userPass: userPass});
});

document.getElementById("submitRegisternBtn").addEventListener("click", function(){
    authInvalid.innerHTML="";
    entireUI.style.display = "none";
    loadingUI.style.display = "flex";
    var userEmail = document.getElementById("userEmail").value;
    port.postMessage({command: 3, userEmail: userEmail});
});

//міняю інтерфейс якщо user авторизувався
function loggedIn(totalData){
  document.getElementById('userName').value = "";
  document.getElementById('userPass').value = "";
  document.getElementById('userEmail').value = "";
  authUI.style.display = "none";
  registerUI.style.display = "none";
  loadingUI.style.display = "none";
    entireUI.style.display = "initial";
  document.getElementById("resultData").innerHTML=totalData;
  companyUI.style.display = "initial";
}
//---------------



//міняю інтерфейс якщо user вийшов
function loggedOut(){
  authUI.style.display = "initial";
  registerUI.style.display = "initial";
  companyUI.style.display = "none";
}
//---------------

//listener для реагування на відповді background.js
port.onMessage.addListener(function(msg){
  switch(msg.response){
    case 0:{
      loggedOut();
      break;
    }
    case 1 : {
      if(msg.userExists)
          port.postMessage({command: 2});
      else
        entireUI.style.display = "initial";
        loadingUI.style.display = "none";
        authInvalid.style.color = "red",
        authInvalid.innerHTML="Invalid user name or password";
      break;
    }
    case 2:{
      loggedIn(msg.domainTotal);
      break;
    }
  }
});
