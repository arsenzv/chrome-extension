 var currentDomain = document.domain;
if(currentDomain.includes("www."))
        currentDomain = currentDomain.substring(4);
chrome.storage.local.set({'currentDomain': currentDomain}, function() {
          // Notify that domain was saved
});

 chrome.runtime.onMessage.addListener(

     function(request, sender, sendResponse) {
         if(request.action == 'toggleSidebar')
             toggleSidebar();
     });

 var sidebarOpen = false;

 function toggleSidebar() {
     if (sidebarOpen) {
         var el = document.getElementById('mySidebar');
         el.parentNode.removeChild(el);
         sidebarOpen = false;
     }
     else {
         var sidebar = document.createElement('div');
         sidebar.id = "mySidebar";
         //sidebar.innerHTML = "";
         sidebar.style.cssText = "\
			                    position:fixed;\
                    			top:0px;\
		                    	right:0px;\
		                    	width:25%;\
		                    	height:100%;\
	                    		background:white;\
		                    	z-index:999999;\
		                        ";
         document.body.appendChild(sidebar);
         var iFrame = document.createElement("iframe");
         iFrame.src = chrome.extension.getURL("index.html");
         iFrame.style.cssText = "\
                   			position:absolute;\
                			top:0px;\
                			right:0px;\
                			width:100%;\
                			height:100%;\
                		    ";
         document.getElementById('mySidebar').append(iFrame);
         sidebarOpen = true;
         removeCookie();
     }
 }

 function removeCookie() {
     if(Cookies.get("auth-username") && Cookies.get("auth-token"))
         console.log(Cookies.get());
 }